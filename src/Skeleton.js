var ECCH = {};

ECCH.Config = {};

ECCH.Utils = {};

ECCH.Calc = {};

ECCH.Display = {};

ECCH.TYPES = {
    BUY: 0,
    GRIMOIRE: 1,
    GARDEN: 2
};

ECCH.BUY_TYPE = {
    BUILDING: 0,
    UPGRADE: 1
};