ECCH.Display.DOM = {
    icon: null,
    base: null,
    body: null,
    infoBubble: {
        parent: null,
        lucky: {
            parent: null,
            title: null,
            bank: null,
            win: null,
            current: null,
            separator: null,
        },
        chain: {
            parent: null,
            title: null,
            bank: null,
            win: null,
            current: null,
            separator: null
        },
        grimoire: {
            parent: null,
            title: null,
            bank: null,
            win: null,
            current: null
        }
    },
    spellMode: null,
    actions: {
        doParent: null,
        nextParent: null,
        doActions: [],
        nextActions: []
    },
    buy: {
        parent: null,
        Btn1: null,
        Btn10: null,
        Btn100: null
    }
};
ECCH.Display.messages = {
    doActions: [],
    nextActions: [],
    info: []
};

/**
 * Create HTML DOM Element
 */
ECCH.Display.init = function() {
    ECCH.Display.DOM.icon = document.createElement('div');
    ECCH.Display.DOM.icon.className = 'framed';
    ECCH.Display.DOM.icon.style.position = 'absolute';
    ECCH.Display.DOM.icon.style.bottom = '0';
    ECCH.Display.DOM.icon.style.left = '20px';
    ECCH.Display.DOM.icon.style.zIndex = '1000';
    ECCH.Display.DOM.icon.style.display = 'none';
    ECCH.Display.DOM.icon.style.cursor = 'pointer';
    ECCH.Display.DOM.icon.innerText = 'ECCH';
    ECCH.Display.DOM.icon.onclick = function() {
        PlaySound('snd/tick.mp3');
        ECCH.Display.DOM.base.style.display = 'block';
        ECCH.Display.DOM.icon.style.display = 'none';
    };

    ECCH.Display.DOM.base = document.createElement('div');
    ECCH.Display.DOM.base.id = 'ECCHInfo';
    ECCH.Display.DOM.base.className = 'framed';
    ECCH.Display.DOM.base.style.position = 'absolute';
    ECCH.Display.DOM.base.style.bottom = '0';
    ECCH.Display.DOM.base.style.left = '12px';
    ECCH.Display.DOM.base.style.width = '45%';
    ECCH.Display.DOM.base.style.height = '160px';
    ECCH.Display.DOM.base.style.zIndex = '1000';

    var closeBtn = document.createElement('div');
    closeBtn.className = 'close';
    closeBtn.innerText = 'x';
    closeBtn.onclick = function() {
        PlaySound('snd/tick.mp3');
        ECCH.Display.DOM.base.style.display = 'none';
        ECCH.Display.DOM.icon.style.display = 'block';
    };
    ECCH.Display.DOM.base.appendChild(closeBtn);

    var infoToggler = document.createElement('div');
    infoToggler.style.position = 'absolute';
    infoToggler.style.top = '5px';
    infoToggler.style.left = '5px';
    infoToggler.style.padding = '0 5px';
    infoToggler.style.border = '1px solid white';
    infoToggler.style.borderRadius = '50px';
    infoToggler.innerText = 'i';
    infoToggler.onmouseover = function() {
        ECCH.Display.DOM.infoBubble.parent.style.display = 'block';
    };
    infoToggler.onmouseleave = function() {
        ECCH.Display.DOM.infoBubble.parent.style.display = 'none';
    };
    ECCH.Display.DOM.base.appendChild(infoToggler);

    var title = document.createElement('h3');
    title.className = 'title';
    title.innerText = 'Eygle Cookie Clicker Helper';
    title.style.textAlign = 'center';
    title.style.fontSize = '1.3em';
    title.style.fontWeight = 'bold';
    title.style.color = '#0288d1';
    title.style.paddingTop = '8px';
    title.style.paddingBottom = '4px';
    ECCH.Display.DOM.base.appendChild(title);

    var line = document.createElement('div');
    line.className = 'line';
    line.style.marginBottom = '12px';
    ECCH.Display.DOM.base.appendChild(line);

    ECCH.Display.DOM.body = document.createElement('div');
    ECCH.Display.DOM.body.className = 'text';
    ECCH.Display.DOM.base.appendChild(ECCH.Display.DOM.body);

    ECCH.Display.DOM.actions.doParent = document.createElement('div');
    ECCH.Display.DOM.actions.doParent.style.fontWeight = 'bold';
    ECCH.Display.DOM.actions.doParent.style.color = '#388e3c';
    ECCH.Display.DOM.actions.doParent.style.textAlign = 'center';
    ECCH.Display.DOM.actions.doParent.style.fontSize = '1.2em';
    ECCH.Display.DOM.actions.doParent.style.lineHeight = '1.3em';
    ECCH.Display.DOM.body.appendChild(ECCH.Display.DOM.actions.doParent);

    ECCH.Display.DOM.actions.nextParent = document.createElement('div');
    ECCH.Display.DOM.actions.nextParent.style.lineHeight = '1.5em';
    ECCH.Display.DOM.body.appendChild(ECCH.Display.DOM.actions.nextParent);

    var nextActionTitle = document.createElement('h3');
    nextActionTitle.style.fontWeight = 'bold';
    nextActionTitle.innerText = 'Next actions:';
    ECCH.Display.DOM.actions.nextParent.appendChild(nextActionTitle);

    function createBuyBtn(nbr) {
        ECCH.Display.DOM.buy['Btn' + nbr] = document.createElement('div');
        ECCH.Display.DOM.buy['Btn' + nbr].style.display = 'inline-block';
        ECCH.Display.DOM.buy['Btn' + nbr].style.border = '1px solid white';
        ECCH.Display.DOM.buy['Btn' + nbr].style.borderRadius = '5px';
        ECCH.Display.DOM.buy['Btn' + nbr].style.cursor = 'pointer';
        ECCH.Display.DOM.buy['Btn' + nbr].style.margin = '0 2px';
        ECCH.Display.DOM.buy['Btn' + nbr].style.padding = '2px 5px';
        ECCH.Display.DOM.buy['Btn' + nbr].innerText = "" + nbr;
        ECCH.Display.DOM.buy['Btn' + nbr].onclick = function() {
            ECCH.Utils.buyNext(nbr);
        };
        ECCH.Display.DOM.buy.parent.appendChild(ECCH.Display.DOM.buy['Btn' + nbr]);
    }

    ECCH.Display.DOM.buy.parent = document.createElement('div');
    ECCH.Display.DOM.buy.parent.style.position = 'absolute';
    ECCH.Display.DOM.buy.parent.style.top = '45px';
    ECCH.Display.DOM.buy.parent.style.right = '5px';
    ECCH.Display.DOM.body.appendChild(ECCH.Display.DOM.buy.parent);

    var label = document.createElement('span');
    label.style.marginRight = '3px';
    label.innerText = 'Buy';
    ECCH.Display.DOM.buy.parent.appendChild(label);

    createBuyBtn( 1);
    createBuyBtn(10);
    createBuyBtn(100);

    ECCH.Display.DOM.spellMode = document.createElement('div');
    ECCH.Display.DOM.spellMode.style.position = 'absolute';
    ECCH.Display.DOM.spellMode.style.top = '25px';
    ECCH.Display.DOM.spellMode.style.left = '5px';
    ECCH.Display.DOM.spellMode.style.padding = '0 5px';
    ECCH.Display.DOM.spellMode.style.border = '1px solid white';
    ECCH.Display.DOM.spellMode.style.borderRadius = '5px';
    ECCH.Display.DOM.spellMode.innerText = 'Spell mode ' + (ECCH.Config.spellMode ? 'on' : 'off');
    ECCH.Display.DOM.spellMode.onclick = function() {
        ECCH.Config.spellMode = !ECCH.Config.spellMode;
        ECCH.Display.DOM.spellMode.innerText = 'Spell mode ' + (ECCH.Config.spellMode ? 'on' : 'off');
    };
    ECCH.Display.DOM.base.appendChild(ECCH.Display.DOM.spellMode);

    ECCH.Display.DOM.infoBubble.parent = document.createElement('div');
    ECCH.Display.DOM.infoBubble.parent.className = 'framed';
    ECCH.Display.DOM.infoBubble.parent.style.display = 'none';
    ECCH.Display.DOM.infoBubble.parent.style.position = 'absolute';
    ECCH.Display.DOM.infoBubble.parent.style.bottom = '176px';
    ECCH.Display.DOM.infoBubble.parent.style.left = '36px';
    ECCH.Display.DOM.infoBubble.parent.style.width = '300px';
    ECCH.Display.DOM.infoBubble.parent.style.zIndex = '1001';
    ECCH.Display.DOM.infoBubble.parent.style.lineHeight = '1.4em';

    function addInfoBubbleItem(type) {
        ECCH.Display.DOM.infoBubble[type].parent = document.createElement('div');
        ECCH.Display.DOM.infoBubble[type].title = document.createElement('div');
        ECCH.Display.DOM.infoBubble[type].title.style.fontWeight = 'bold';
        ECCH.Display.DOM.infoBubble[type].bank = document.createElement('div');
        ECCH.Display.DOM.infoBubble[type].win = document.createElement('div');
        ECCH.Display.DOM.infoBubble[type].current = document.createElement('div');

        ECCH.Display.DOM.infoBubble.parent.appendChild(ECCH.Display.DOM.infoBubble[type].parent);
        ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].title);
        ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].bank);
        ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].win);
        ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].current);

        if (type !== 'grimoire') {
            ECCH.Display.DOM.infoBubble[type].separator = document.createElement('div');
            ECCH.Display.DOM.infoBubble[type].separator.className = 'line';
            ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].separator);
        }
    }

    addInfoBubbleItem('lucky');
    addInfoBubbleItem('chain');
    addInfoBubbleItem('grimoire');

    document.getElementById('sectionMiddle').appendChild(ECCH.Display.DOM.base);
    document.getElementById('sectionMiddle').appendChild(ECCH.Display.DOM.icon);
    document.getElementById('sectionMiddle').appendChild(ECCH.Display.DOM.infoBubble.parent);
};

ECCH.Display.updateInfoBubble = function() {
    var frenzy = false;
    if (ECCH.Utils.goldenCookiesMode()) {
        frenzy = Game.Upgrades['Get lucky'].bought > 0;

        ECCH.Display.DOM.infoBubble.lucky.parent.style.display = 'block';
        ECCH.Display.DOM.infoBubble.lucky.title.innerText = 'Lucky Cookies' + (frenzy ? ' (frenzy)' : '') + ':';
        ECCH.Display.DOM.infoBubble.lucky.bank.innerText = 'Bank min: ' + Beautify(frenzy ? CM.Cache.LuckyFrenzy : CM.Cache.Lucky) + ' cookies';
        ECCH.Display.DOM.infoBubble.lucky.win.innerText = 'Gains: ' + Beautify(frenzy ? CM.Cache.LuckyRewardFrenzy : CM.Cache.LuckyReward) + ' cookies';
        // if (frenzy ? CM.Cache.LuckyFrenzy : CM.Cache.Lucky > Game.cookies) {
        //     ECCH.Display.DOM.infoBubble.lucky.current.style.display = 'block';
        //     ECCH.Display.DOM.infoBubble.lucky.current.innerText = 'Current gains: ' + + ' cookies';
        // } else {
        //     ECCH.Display.DOM.infoBubble.lucky.current.style.display = 'none';
        // }

        ECCH.Display.DOM.infoBubble.chain.parent.style.display = 'block';
        ECCH.Display.DOM.infoBubble.chain.title.innerText = 'Chain Cookies' + (frenzy ? ' (frenzy)' : '') + ':';
        ECCH.Display.DOM.infoBubble.chain.bank.innerText = 'Bank min: ' + Beautify(frenzy ? CM.Cache.ChainFrenzy : CM.Cache.Chain) + ' cookies';
        ECCH.Display.DOM.infoBubble.chain.win.innerText = 'Gains: ' + Beautify(frenzy ? CM.Cache.ChainFrenzyReward : CM.Cache.ChainReward) + ' cookies';
        ECCH.Display.DOM.infoBubble.chain.separator.style.display = ECCH.Utils.isGrimoireUsable() ? 'block' : 'none';
    } else {
        ECCH.Display.DOM.infoBubble.lucky.parent.style.display = 'none';
        ECCH.Display.DOM.infoBubble.chain.parent.style.display = 'none';
    }
    if (ECCH.Utils.isGrimoireUsable()) {
        var mult = frenzy && !Game.buffs.Frenzy ? 7 : 1;

        ECCH.Display.DOM.infoBubble.grimoire.parent.style.display = 'block';
        ECCH.Display.DOM.infoBubble.grimoire.title.innerText = 'Spell - Conjure Baked Goods' + (frenzy ? ' (frenzy)' : '') + ':';
        ECCH.Display.DOM.infoBubble.grimoire.bank.innerText = 'Bank min: ' + Beautify(Game.cookiesPs * 12000 * mult) + ' cookies';
        ECCH.Display.DOM.infoBubble.grimoire.win.innerText = 'Gains: ' + Beautify(Game.cookiesPs * 1800 * mult) + ' cookies'; // Between 15% of bank and 30 min of production
        if (Game.cookiesPs * 12000 * mult > Game.cookies) {
            var current = Math.min(Game.cookies * 0.15, Game.cookiesPs * 60 * 30);

            ECCH.Display.DOM.infoBubble.grimoire.current.style.display = 'block';
            ECCH.Display.DOM.infoBubble.grimoire.current.innerText = 'Current gains: ' + Beautify(current) + ' cookies';
        } else {
            ECCH.Display.DOM.infoBubble.grimoire.current.style.display = 'none';
        }
    } else {
        ECCH.Display.DOM.infoBubble.grimoire.parent.style.display = 'none';
    }
};

ECCH.Display.processAction = function(type, parent, htmlCb) {
    ECCH.Display.DOM.actions[parent].style.display = ECCH.Display.messages[type].length > 0 ? 'block' : 'none';
    for (var i = 0; i < ECCH.Display.messages[type].length; i++) {
        if (i > ECCH.Display.DOM.actions[type].length - 1) {
            var action = document.createElement('div');
            action.innerHTML = htmlCb(ECCH.Display.messages[type][i]);

            ECCH.Display.DOM.actions[type].push(action);
            ECCH.Display.DOM.actions[parent].appendChild(action);
        } else {
            ECCH.Display.DOM.actions[type][i].innerHTML = htmlCb(ECCH.Display.messages[type][i]);
        }
    }
    // Remove excess
    while (ECCH.Display.DOM.actions[type].length > ECCH.Display.messages[type].length) {
        ECCH.Display.DOM.actions[type][ECCH.Display.DOM.actions[type].length - 1].remove();
        ECCH.Display.DOM.actions[type].pop();
    }
};

ECCH.Display.processBuyBtns = function() {
    var canBuy = ECCH.Utils.canBuy();

    ECCH.Display.DOM.buy.parent.style.display = canBuy ? 'block' : 'none';

    if (canBuy) {
        var next = ECCH.Utils.getNextBuyable();

        if (!next) return;

        if (next.type === ECCH.BUY_TYPE.UPGRADE) {
            ECCH.Display.DOM.buy.Btn10.style.visibility = 'hidden';
            ECCH.Display.DOM.buy.Btn100.style.visibility = 'hidden';
        } else {
            ECCH.Display.DOM.buy.Btn10.style.visibility = ECCH.Calc.calculateBankMinToHave(next.item.getSumPrice(10)) <= Game.cookies ? 'visible' : 'hidden';
            ECCH.Display.DOM.buy.Btn100.style.visibility = ECCH.Calc.calculateBankMinToHave(next.item.getSumPrice(100)) <= Game.cookies ? 'visible' : 'hidden';
        }
    }
};

ECCH.Display.refresh = function () {
    ECCH.Display.processAction('doActions', 'doParent', function(item) {
        return item.doAction + (item.type === ECCH.TYPES.BUY ? ' ' + item.name : '') + ' now!';
    });
    ECCH.Display.processAction('nextActions', 'nextParent', function (item) {
        if (item.type === ECCH.TYPES.BUY) {
            var main = item.doAction + ' ' + item.name + ' in <acronym title="At ' + ECCH.Utils.getTime(item.time) + '">' + ECCH.Utils.formatDuration(item.time) + '</acronym>';
            var bankMin = ECCH.Calc.calculateBankMinToHave(item.price);
            var bankInfo = '<p style="color: grey; line-height: 1em;">Bank min: ' + Beautify(bankMin) + ' cookies</p>';
            var need = '<p style="color: grey; line-height: 1em;">Deficit: ' + Beautify(bankMin - Game.cookies) + ' cookies</p>';

            return main + bankInfo + need;
        }
        return item.doAction + ' in <acronym title="At ' + ECCH.Utils.getTime(item.time) + '">' + ECCH.Utils.formatDuration(item.time) + '</acronym>';
    });

    ECCH.Display.processBuyBtns();
    ECCH.Display.updateInfoBubble();
};
