/**
 * Get next item to perform action on
 * @returns {{type: number, time: number, doAction: string}}
 */
ECCH.Calc.getItemsSortedByTime = function() {
    var times = [];
    var lowestBuy = ECCH.Calc.getLowestPP();
    var gardenItem = ECCH.Calc.checkGardenState();

    if (ECCH.Utils.isGrimoireUsable()) {
        times.push({
            type: ECCH.TYPES.GRIMOIRE,
            time: ECCH.Calc.getGrimoireRefillTime(),
            doAction: 'Cast spell'
        });
    }
    times.push({
        type: ECCH.TYPES.BUY,
        time: ECCH.Calc.calculateTimeToGet(lowestBuy.getPrice()),
        price: lowestBuy.getPrice(),
        name: lowestBuy.name,
        doAction: 'Buy'
    });

    if (gardenItem) {
        times.push(gardenItem);
    }

    return times.sort(ECCH.Utils.sortByKey('time'));
};

/**
 * Calculate time to get given cookies taking in consideration the bank minimum
 **/
ECCH.Calc.calculateTimeToGet = function(cost) {
    var rest = ECCH.Calc.calculateBankMinToHave(cost) - Game.cookies;

    return rest <= 0 ? 0 : rest / ECCH.Utils.getRealCps();
};

/**
 * Calculate how many cookies should be in bank to buy item at given cost
 * @param cost
 * @returns {number}
 */
ECCH.Calc.calculateBankMinToHave = function(cost) {
    var spellMin = Game.cookiesPs * 12000;
    var min = 0;

    if (ECCH.Utils.isGrimoireUsable()) {
        min = spellMin;
    }
    if (ECCH.Utils.goldenCookiesMode()) {
        min = Math.max(CM.Cache.Lucky, CM.Cache.Chain, min);

        if (Math.max(Game.Upgrades['Get lucky'].bought > 0)) {
            min = Math.max(CM.Cache.LuckyFrenzy, CM.Cache.ChainFrenzy, min);

            if (ECCH.Utils.isGrimoireUsable() && !Game.buffs.Frenzy) {
                min = Math.max(spellMin * 7, min);
            }
        }
    }

    return cost + min;
};



/**
 * Use CookieMonster data to get lower PP object or Upgrade cost
 * TODO es5 compliant
 */
ECCH.Calc.getLowestPP = function() {
    function findLowestPP(list, names) {
        var lower = null;

        for (const name of Object.keys(list)) {
            if ((!names || names.includes(name)) && (!lower || lower.pp > list[name].pp)) {
                lower = {
                    name,
                    pp: list[name].pp
                };
            }
        }

        return lower;
    }

    var excludes = ['one mind', 'communal brainsweep', 'elder pact', 'elder pledge', 'elder covenant',
        'revoke elder covenant', 'golden switch [off]', 'golden switch [on]',
        'festive biscuit', 'ghostly biscuit', 'lovesick biscuit', 'fool\'s biscuit', 'bunny biscuit'];
    var lowerObject = findLowestPP(CM.Cache.Objects);
    var lowerUpgrade = findLowestPP(CM.Cache.Upgrades, Game.UpgradesInStore
        .filter(u => !excludes.includes(u.name.toLowerCase())).map(u => u.name));

    if (!lowerUpgrade || lowerObject.pp < lowerUpgrade.pp) {
        return Game.ObjectsById.find(e => lowerObject.name === e.name);
    }

    return Game.UpgradesById.find(e => lowerUpgrade.name === e.name);
};

/**
 * Get number of seconds until grimoire refill
 */
ECCH.Calc.getGrimoireRefillTime = function() {
    var game = Game.Objects['Wizard tower'].minigame;
    var currentMagic = game.magic;
    var maxMagic = game.magicM;
    var targetMagic = game.magicM;
    var count = 0;

    while (currentMagic < targetMagic) {
        currentMagic += Math.max(0.002, Math.pow(currentMagic / Math.max(maxMagic, 100), 0.5)) * 0.002;
        count++;
    }

    var refillIn = count / Game.fps;

    // Spell backfired, we have to wait until the malus is done
    if (Game.buffs['Magic inept']) {
        refillIn = Math.max(refillIn, Game.buffs['Magic inept'].time / Game.fps);
    }

    return Math.max(parseInt(refillIn, 10), 0);
};

/**
 * Check garden state
 * @returns {null|Object} item to push
 */
ECCH.Calc.checkGardenState = function() {
    if (!Game.Objects['Farm'].minigameLoaded || Game.Objects['Farm'].bought < 100) {
        return null;
    }

    var game = Game.Objects['Farm'].minigame;
    var emptySlots = 0;
    var matureSlots = 0;
    var nbrSlots = 0;

    for (var x = 0; x < 6; x++) {
        for (var y = 0; y < 6; y++) {
            if (game.isTileUnlocked(x, y)) {
                var tile = game.getTile(x, y);

                if (tile[0]) {
                    var plant = game.plantsById[tile[0] - 1];

                    // plant is growing
                    if (plant.unlocked) {
                        // plant is mature
                        if (tile[1] >= game.plantsById[tile[0] - 1].mature) {
                            matureSlots++;
                        }
                    }
                } else {
                    emptySlots++;
                }
                nbrSlots++;
            }
        }
    }

    var item = {
        type: ECCH.TYPES.GARDEN,
        time: 0
    };

    if (emptySlots > 0 && matureSlots < nbrSlots / 2) {
        // (near) empty garden
        item.doAction = 'Plant garden';
    } else if (emptySlots === 0 && matureSlots > 0 && game.soilsById[game.soil].name !== 'Clay') {
        // mature garden
        item.doAction = 'Change garden soil';
    }

    return item.doAction ? item : null;
};
