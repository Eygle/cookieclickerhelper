/**
 * Loop
 */
ECCH.loop = function () {
    var list = ECCH.Calc.getItemsSortedByTime();
    var hasPlayed = {};

    hasPlayed[ECCH.TYPES.BUY] = false;
    hasPlayed[ECCH.TYPES.GRIMOIRE] = false;
    hasPlayed[ECCH.TYPES.GARDEN] = false;

    ECCH.Display.messages.doActions = [];
    ECCH.Display.messages.nextActions = [];

    list.forEach(function (item) {
        if (item.time > 0) {
            ECCH.Display.messages.nextActions.push(item);
        } else {
            ECCH.Utils.playSound(item.type);
            hasPlayed[item.type] = true;
            ECCH.Display.messages.doActions.push(item);
        }
    });

    Object.keys(hasPlayed).forEach(function (type) {
        type = parseInt(type, 10);
        if (!hasPlayed[type]) {
            ECCH.Utils.getSoundByType(type).hasPlayed = false;
        }
    });

    ECCH.Display.refresh();
    setTimeout(ECCH.loop, ECCH.Config.INTERVAL);
};

ECCH.Config.reset();

ECCH.runDelayed = function () {
    if (typeof CM === 'object' && typeof CM.Cache === 'object'
        && typeof CM.Cache.Objects === 'object' && typeof CM.Cache.Upgrades === 'object') {
        ECCH.Display.init();
        ECCH.loop();
        console.log('Eygle Cookie Clicker Helper mod is loaded!');
    } else {
        setTimeout(ECCH.runDelayed, 500);
    }
};

ECCH.runDelayed();