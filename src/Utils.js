ECCH.Utils.getSoundByType = function (type) {
    switch (type) {
        case ECCH.TYPES.BUY:
            return ECCH.Config.Sounds.Buy;
        case ECCH.TYPES.GARDEN:
            return ECCH.Config.Sounds.Garden;
        case ECCH.TYPES.GRIMOIRE:
            return ECCH.Config.Sounds.Grimoire;
    }
    return {};
};

/**
 * Play notification sound
 */
ECCH.Utils.playSound = function (type) {
    var sound = ECCH.Utils.getSoundByType(type);

    if (sound.volume === 0 || sound.hasPlayed) return;

    var player = new realAudio(sound.url);

    sound.hasPlayed = true;
    player.volume = sound.volume / 100;
    player.play();
};

/**
 * Sort array of object by key
 * @param key
 * @returns {function(*, *): number}
 */
ECCH.Utils.sortByKey = function (key) {
    return (a, b) => a[key] > b[key] ? 1 : (a[key] < b[key] ? -1 : 0);
};

/**
 * format duration
 * @param s
 * @returns {string}
 */
ECCH.Utils.formatDuration = function (s) {
    if (isNaN(s) || !isFinite(s)) {
        return '∞';
    }

    if (s > 86400) {
        return '>' + parseInt(s / 86400) + ' days'
    }

    var date = new Date(null);

    date.setSeconds(s);

    return date.toISOString().substr(11, 8);
};

/**
 * Get real time
 * @param s
 * @returns {string}
 */
ECCH.Utils.getTime = function (s) {
    if (isNaN(s) || !isFinite(s)) {
        return '∞';
    }

    var t = new Date();

    function format(n) {
        return n < 10 ? '0' + n : n;
    }

    t.setSeconds(t.getSeconds() + s);

    if (s > 86400) {
        return t.toLocaleString();
    }

    return format(t.getHours()) + ':' + format(t.getMinutes()) + ':' + format(t.getSeconds());
};

/**
 * Get real CPS including wrinklers
 * @returns {number}
 */
ECCH.Utils.getRealCps = function () {
    return (Game.cookiesPs * (1 - Game.cpsSucked));
};

/**
 * is grimoire usable
 * @returns {*|boolean}
 */
ECCH.Utils.isGrimoireUsable = function () {
    if (!ECCH.Config.spellMode) {
        return false;
    }
    var grimoire = Game.Objects['Wizard tower'].minigame;

    if (!grimoire) {
        return false;
    }
    var minMagicNeeded = grimoire.getSpellCost(grimoire.spells['conjure baked goods']) +
        grimoire.getSpellCost(grimoire.spells['diminish ineptitude']);

    return Game.Objects['Wizard tower'].minigameLoaded && Game.Objects['Wizard tower'].bought > 0
        && grimoire.magicM >= minMagicNeeded;
};

/**
 * Is golden cookies mode enabled?
 * @returns {boolean}
 */
ECCH.Utils.goldenCookiesMode = function () {
    var ascetismSloted = Game.Objects['Temple'].minigameLoaded && Game.Objects['Temple'].bought > 0
        && Game.Objects['Temple'].minigame.gods.asceticism.slot !== -1;
    var shimeringVeilActive = Game.Upgrades["Shimmering veil [on]"]
        && Game.Upgrades["Shimmering veil [on]"].unlocked > 0;
    var goldenSwitchActive = Game.Upgrades["Golden switch [on]"]
        && Game.Upgrades["Golden switch [on]"].unlocked > 0;

    return !ascetismSloted && !shimeringVeilActive && !goldenSwitchActive;
};

/**
 * Can buy next item?
 */
ECCH.Utils.canBuy = function () {
    var isSpellInDoAction = false;

    for (var i = 0; i < ECCH.Display.messages.doActions.length; i++) {
        if (ECCH.Display.messages.doActions[i].type === ECCH.TYPES.BUY) {
            return true;
        } else if (ECCH.Display.messages.doActions[i].type === ECCH.TYPES.GRIMOIRE) {
            isSpellInDoAction = true;
        }
    }

    if (isSpellInDoAction) {
        return false;
    }

    if (ECCH.Display.messages.nextActions.length > 0) {
        if (ECCH.Display.messages.nextActions[0].type === ECCH.TYPES.BUY) {
            return true;
        }
    }

    return false;
};

/**
 * Buy next item if possible
 */
ECCH.Utils.buyNext = function (nbr) {
    function doBuy(item) {
        if (item.type === ECCH.TYPES.BUY) {
            if (Game.Objects[item.name]) {
                Game.Objects[item.name].buy(nbr);
                return true;
            } else if (Game.Upgrades[item.name]) {
                Game.Upgrades[item.name].buy(nbr);
                return true;
            }
        }
        return false;
    }

    for (var i = 0; i < ECCH.Display.messages.doActions.length; i++) {
        if (doBuy(ECCH.Display.messages.doActions[i])) {
            return;
        }
    }

    if (ECCH.Display.messages.nextActions.length > 0) {
        doBuy(ECCH.Display.messages.nextActions[0]);
    }
};

/**
 * Get next item you can buy
 */
ECCH.Utils.getNextBuyable = function() {
    function doGet(item) {
        if (item.type === ECCH.TYPES.BUY) {
            if (Game.Objects[item.name]) {
                return { type: ECCH.BUY_TYPE.BUILDING, item: Game.Objects[item.name]};
            } else if (Game.Upgrades[item.name]) {
                return { type: ECCH.BUY_TYPE.UPGRADE, item: Game.Upgrades[item.name]};
            }
            return null;
        }
    }

    for (var i = 0; i < ECCH.Display.messages.doActions.length; i++) {
        var item = doGet(ECCH.Display.messages.doActions[i]);
        if (item) {
            return item;
        }
    }

    if (ECCH.Display.messages.nextActions.length > 0) {
        return doGet(ECCH.Display.messages.nextActions[0]);
    }
};
