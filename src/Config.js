ECCH.Config.INTERVAL = parseInt(1000 / 24, 10);
ECCH.Config.Displayed = true;
ECCH.Config.Sounds = {
    Buy: {
        hasPlayed: false,
    },
    Garden: {
        hasPlayed: false,
    },
    Grimoire: {
        hasPlayed: false,
    }
};
ECCH.Config.spellMode = true;

ECCH.Config.reset = function() {
    ECCH.Config.spellMode = true;
    ECCH.Config.Sounds.Buy.url = 'https://freesound.org/data/previews/316/316798_5383582-lq.mp3';
    ECCH.Config.Sounds.Buy.volume = 100;
    ECCH.Config.Sounds.Garden.url = 'https://freesound.org/data/previews/223/223352_3184722-lq.mp3';
    ECCH.Config.Sounds.Garden.volume = 100;
    ECCH.Config.Sounds.Grimoire.url = 'https://freesound.org/data/previews/342/342432_321967-lq.mp3';
    ECCH.Config.Sounds.Grimoire.volume = 100;
};
