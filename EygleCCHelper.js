var ECCH = {};

ECCH.Config = {};

ECCH.Utils = {};

ECCH.Calc = {};

ECCH.Display = {};

ECCH.TYPES = {
    BUY: 0,
    GRIMOIRE: 1,
    GARDEN: 2
};

ECCH.BUY_TYPE = {
    BUILDING: 0,
    UPGRADE: 1
};ECCH.Config.INTERVAL = parseInt(1000 / 24, 10);
ECCH.Config.Displayed = true;
ECCH.Config.Sounds = {
    Buy: {
        hasPlayed: false,
    },
    Garden: {
        hasPlayed: false,
    },
    Grimoire: {
        hasPlayed: false,
    }
};
ECCH.Config.spellMode = true;

ECCH.Config.reset = function() {
    ECCH.Config.spellMode = true;
    ECCH.Config.Sounds.Buy.url = 'https://freesound.org/data/previews/316/316798_5383582-lq.mp3';
    ECCH.Config.Sounds.Buy.volume = 100;
    ECCH.Config.Sounds.Garden.url = 'https://freesound.org/data/previews/223/223352_3184722-lq.mp3';
    ECCH.Config.Sounds.Garden.volume = 100;
    ECCH.Config.Sounds.Grimoire.url = 'https://freesound.org/data/previews/342/342432_321967-lq.mp3';
    ECCH.Config.Sounds.Grimoire.volume = 100;
};
ECCH.Utils.getSoundByType = function (type) {
    switch (type) {
        case ECCH.TYPES.BUY:
            return ECCH.Config.Sounds.Buy;
        case ECCH.TYPES.GARDEN:
            return ECCH.Config.Sounds.Garden;
        case ECCH.TYPES.GRIMOIRE:
            return ECCH.Config.Sounds.Grimoire;
    }
    return {};
};

/**
 * Play notification sound
 */
ECCH.Utils.playSound = function (type) {
    var sound = ECCH.Utils.getSoundByType(type);

    if (sound.volume === 0 || sound.hasPlayed) return;

    var player = new realAudio(sound.url);

    sound.hasPlayed = true;
    player.volume = sound.volume / 100;
    player.play();
};

/**
 * Sort array of object by key
 * @param key
 * @returns {function(*, *): number}
 */
ECCH.Utils.sortByKey = function (key) {
    return (a, b) => a[key] > b[key] ? 1 : (a[key] < b[key] ? -1 : 0);
};

/**
 * format duration
 * @param s
 * @returns {string}
 */
ECCH.Utils.formatDuration = function (s) {
    if (isNaN(s) || !isFinite(s)) {
        return '∞';
    }

    if (s > 86400) {
        return '>' + parseInt(s / 86400) + ' days'
    }

    var date = new Date(null);

    date.setSeconds(s);

    return date.toISOString().substr(11, 8);
};

/**
 * Get real time
 * @param s
 * @returns {string}
 */
ECCH.Utils.getTime = function (s) {
    if (isNaN(s) || !isFinite(s)) {
        return '∞';
    }

    var t = new Date();

    function format(n) {
        return n < 10 ? '0' + n : n;
    }

    t.setSeconds(t.getSeconds() + s);

    if (s > 86400) {
        return t.toLocaleString();
    }

    return format(t.getHours()) + ':' + format(t.getMinutes()) + ':' + format(t.getSeconds());
};

/**
 * Get real CPS including wrinklers
 * @returns {number}
 */
ECCH.Utils.getRealCps = function () {
    return (Game.cookiesPs * (1 - Game.cpsSucked));
};

/**
 * is grimoire usable
 * @returns {*|boolean}
 */
ECCH.Utils.isGrimoireUsable = function () {
    if (!ECCH.Config.spellMode) {
        return false;
    }
    var grimoire = Game.Objects['Wizard tower'].minigame;

    if (!grimoire) {
        return false;
    }
    var minMagicNeeded = grimoire.getSpellCost(grimoire.spells['conjure baked goods']) +
        grimoire.getSpellCost(grimoire.spells['diminish ineptitude']);

    return Game.Objects['Wizard tower'].minigameLoaded && Game.Objects['Wizard tower'].bought > 0
        && grimoire.magicM >= minMagicNeeded;
};

/**
 * Is golden cookies mode enabled?
 * @returns {boolean}
 */
ECCH.Utils.goldenCookiesMode = function () {
    var ascetismSloted = Game.Objects['Temple'].minigameLoaded && Game.Objects['Temple'].bought > 0
        && Game.Objects['Temple'].minigame.gods.asceticism.slot !== -1;
    var shimeringVeilActive = Game.Upgrades["Shimmering veil [on]"]
        && Game.Upgrades["Shimmering veil [on]"].unlocked > 0;
    var goldenSwitchActive = Game.Upgrades["Golden switch [on]"]
        && Game.Upgrades["Golden switch [on]"].unlocked > 0;

    return !ascetismSloted && !shimeringVeilActive && !goldenSwitchActive;
};

/**
 * Can buy next item?
 */
ECCH.Utils.canBuy = function () {
    var isSpellInDoAction = false;

    for (var i = 0; i < ECCH.Display.messages.doActions.length; i++) {
        if (ECCH.Display.messages.doActions[i].type === ECCH.TYPES.BUY) {
            return true;
        } else if (ECCH.Display.messages.doActions[i].type === ECCH.TYPES.GRIMOIRE) {
            isSpellInDoAction = true;
        }
    }

    if (isSpellInDoAction) {
        return false;
    }

    if (ECCH.Display.messages.nextActions.length > 0) {
        if (ECCH.Display.messages.nextActions[0].type === ECCH.TYPES.BUY) {
            return true;
        }
    }

    return false;
};

/**
 * Buy next item if possible
 */
ECCH.Utils.buyNext = function (nbr) {
    function doBuy(item) {
        if (item.type === ECCH.TYPES.BUY) {
            if (Game.Objects[item.name]) {
                Game.Objects[item.name].buy(nbr);
                return true;
            } else if (Game.Upgrades[item.name]) {
                Game.Upgrades[item.name].buy(nbr);
                return true;
            }
        }
        return false;
    }

    for (var i = 0; i < ECCH.Display.messages.doActions.length; i++) {
        if (doBuy(ECCH.Display.messages.doActions[i])) {
            return;
        }
    }

    if (ECCH.Display.messages.nextActions.length > 0) {
        doBuy(ECCH.Display.messages.nextActions[0]);
    }
};

/**
 * Get next item you can buy
 */
ECCH.Utils.getNextBuyable = function() {
    function doGet(item) {
        if (item.type === ECCH.TYPES.BUY) {
            if (Game.Objects[item.name]) {
                return { type: ECCH.BUY_TYPE.BUILDING, item: Game.Objects[item.name]};
            } else if (Game.Upgrades[item.name]) {
                return { type: ECCH.BUY_TYPE.UPGRADE, item: Game.Upgrades[item.name]};
            }
            return null;
        }
    }

    for (var i = 0; i < ECCH.Display.messages.doActions.length; i++) {
        var item = doGet(ECCH.Display.messages.doActions[i]);
        if (item) {
            return item;
        }
    }

    if (ECCH.Display.messages.nextActions.length > 0) {
        return doGet(ECCH.Display.messages.nextActions[0]);
    }
};
/**
 * Get next item to perform action on
 * @returns {{type: number, time: number, doAction: string}}
 */
ECCH.Calc.getItemsSortedByTime = function() {
    var times = [];
    var lowestBuy = ECCH.Calc.getLowestPP();
    var gardenItem = ECCH.Calc.checkGardenState();

    if (ECCH.Utils.isGrimoireUsable()) {
        times.push({
            type: ECCH.TYPES.GRIMOIRE,
            time: ECCH.Calc.getGrimoireRefillTime(),
            doAction: 'Cast spell'
        });
    }
    times.push({
        type: ECCH.TYPES.BUY,
        time: ECCH.Calc.calculateTimeToGet(lowestBuy.getPrice()),
        price: lowestBuy.getPrice(),
        name: lowestBuy.name,
        doAction: 'Buy'
    });

    if (gardenItem) {
        times.push(gardenItem);
    }

    return times.sort(ECCH.Utils.sortByKey('time'));
};

/**
 * Calculate time to get given cookies taking in consideration the bank minimum
 **/
ECCH.Calc.calculateTimeToGet = function(cost) {
    var rest = ECCH.Calc.calculateBankMinToHave(cost) - Game.cookies;

    return rest <= 0 ? 0 : rest / ECCH.Utils.getRealCps();
};

/**
 * Calculate how many cookies should be in bank to buy item at given cost
 * @param cost
 * @returns {number}
 */
ECCH.Calc.calculateBankMinToHave = function(cost) {
    var spellMin = Game.cookiesPs * 12000;
    var min = 0;

    if (ECCH.Utils.isGrimoireUsable()) {
        min = spellMin;
    }
    if (ECCH.Utils.goldenCookiesMode()) {
        min = Math.max(CM.Cache.Lucky, CM.Cache.Chain, min);

        if (Math.max(Game.Upgrades['Get lucky'].bought > 0)) {
            min = Math.max(CM.Cache.LuckyFrenzy, CM.Cache.ChainFrenzy, min);

            if (ECCH.Utils.isGrimoireUsable() && !Game.buffs.Frenzy) {
                min = Math.max(spellMin * 7, min);
            }
        }
    }

    return cost + min;
};



/**
 * Use CookieMonster data to get lower PP object or Upgrade cost
 * TODO es5 compliant
 */
ECCH.Calc.getLowestPP = function() {
    function findLowestPP(list, names) {
        var lower = null;

        for (const name of Object.keys(list)) {
            if ((!names || names.includes(name)) && (!lower || lower.pp > list[name].pp)) {
                lower = {
                    name,
                    pp: list[name].pp
                };
            }
        }

        return lower;
    }

    var excludes = ['one mind', 'communal brainsweep', 'elder pact', 'elder pledge', 'elder covenant',
        'revoke elder covenant', 'golden switch [off]', 'golden switch [on]',
        'festive biscuit', 'ghostly biscuit', 'lovesick biscuit', 'fool\'s biscuit', 'bunny biscuit'];
    var lowerObject = findLowestPP(CM.Cache.Objects);
    var lowerUpgrade = findLowestPP(CM.Cache.Upgrades, Game.UpgradesInStore
        .filter(u => !excludes.includes(u.name.toLowerCase())).map(u => u.name));

    if (!lowerUpgrade || lowerObject.pp < lowerUpgrade.pp) {
        return Game.ObjectsById.find(e => lowerObject.name === e.name);
    }

    return Game.UpgradesById.find(e => lowerUpgrade.name === e.name);
};

/**
 * Get number of seconds until grimoire refill
 */
ECCH.Calc.getGrimoireRefillTime = function() {
    var game = Game.Objects['Wizard tower'].minigame;
    var currentMagic = game.magic;
    var maxMagic = game.magicM;
    var targetMagic = game.magicM;
    var count = 0;

    while (currentMagic < targetMagic) {
        currentMagic += Math.max(0.002, Math.pow(currentMagic / Math.max(maxMagic, 100), 0.5)) * 0.002;
        count++;
    }

    var refillIn = count / Game.fps;

    // Spell backfired, we have to wait until the malus is done
    if (Game.buffs['Magic inept']) {
        refillIn = Math.max(refillIn, Game.buffs['Magic inept'].time / Game.fps);
    }

    return Math.max(parseInt(refillIn, 10), 0);
};

/**
 * Check garden state
 * @returns {null|Object} item to push
 */
ECCH.Calc.checkGardenState = function() {
    if (!Game.Objects['Farm'].minigameLoaded || Game.Objects['Farm'].bought < 100) {
        return null;
    }

    var game = Game.Objects['Farm'].minigame;
    var emptySlots = 0;
    var matureSlots = 0;
    var nbrSlots = 0;

    for (var x = 0; x < 6; x++) {
        for (var y = 0; y < 6; y++) {
            if (game.isTileUnlocked(x, y)) {
                var tile = game.getTile(x, y);

                if (tile[0]) {
                    var plant = game.plantsById[tile[0] - 1];

                    // plant is growing
                    if (plant.unlocked) {
                        // plant is mature
                        if (tile[1] >= game.plantsById[tile[0] - 1].mature) {
                            matureSlots++;
                        }
                    }
                } else {
                    emptySlots++;
                }
                nbrSlots++;
            }
        }
    }

    var item = {
        type: ECCH.TYPES.GARDEN,
        time: 0
    };

    if (emptySlots > 0 && matureSlots < nbrSlots / 2) {
        // (near) empty garden
        item.doAction = 'Plant garden';
    } else if (emptySlots === 0 && matureSlots > 0 && game.soilsById[game.soil].name !== 'Clay') {
        // mature garden
        item.doAction = 'Change garden soil';
    }

    return item.doAction ? item : null;
};
ECCH.Display.DOM = {
    icon: null,
    base: null,
    body: null,
    infoBubble: {
        parent: null,
        lucky: {
            parent: null,
            title: null,
            bank: null,
            win: null,
            current: null,
            separator: null,
        },
        chain: {
            parent: null,
            title: null,
            bank: null,
            win: null,
            current: null,
            separator: null
        },
        grimoire: {
            parent: null,
            title: null,
            bank: null,
            win: null,
            current: null
        }
    },
    spellMode: null,
    actions: {
        doParent: null,
        nextParent: null,
        doActions: [],
        nextActions: []
    },
    buy: {
        parent: null,
        Btn1: null,
        Btn10: null,
        Btn100: null
    }
};
ECCH.Display.messages = {
    doActions: [],
    nextActions: [],
    info: []
};

/**
 * Create HTML DOM Element
 */
ECCH.Display.init = function() {
    ECCH.Display.DOM.icon = document.createElement('div');
    ECCH.Display.DOM.icon.className = 'framed';
    ECCH.Display.DOM.icon.style.position = 'absolute';
    ECCH.Display.DOM.icon.style.bottom = '0';
    ECCH.Display.DOM.icon.style.left = '20px';
    ECCH.Display.DOM.icon.style.zIndex = '1000';
    ECCH.Display.DOM.icon.style.display = 'none';
    ECCH.Display.DOM.icon.style.cursor = 'pointer';
    ECCH.Display.DOM.icon.innerText = 'ECCH';
    ECCH.Display.DOM.icon.onclick = function() {
        PlaySound('snd/tick.mp3');
        ECCH.Display.DOM.base.style.display = 'block';
        ECCH.Display.DOM.icon.style.display = 'none';
    };

    ECCH.Display.DOM.base = document.createElement('div');
    ECCH.Display.DOM.base.id = 'ECCHInfo';
    ECCH.Display.DOM.base.className = 'framed';
    ECCH.Display.DOM.base.style.position = 'absolute';
    ECCH.Display.DOM.base.style.bottom = '0';
    ECCH.Display.DOM.base.style.left = '12px';
    ECCH.Display.DOM.base.style.width = '45%';
    ECCH.Display.DOM.base.style.height = '160px';
    ECCH.Display.DOM.base.style.zIndex = '1000';

    var closeBtn = document.createElement('div');
    closeBtn.className = 'close';
    closeBtn.innerText = 'x';
    closeBtn.onclick = function() {
        PlaySound('snd/tick.mp3');
        ECCH.Display.DOM.base.style.display = 'none';
        ECCH.Display.DOM.icon.style.display = 'block';
    };
    ECCH.Display.DOM.base.appendChild(closeBtn);

    var infoToggler = document.createElement('div');
    infoToggler.style.position = 'absolute';
    infoToggler.style.top = '5px';
    infoToggler.style.left = '5px';
    infoToggler.style.padding = '0 5px';
    infoToggler.style.border = '1px solid white';
    infoToggler.style.borderRadius = '50px';
    infoToggler.innerText = 'i';
    infoToggler.onmouseover = function() {
        ECCH.Display.DOM.infoBubble.parent.style.display = 'block';
    };
    infoToggler.onmouseleave = function() {
        ECCH.Display.DOM.infoBubble.parent.style.display = 'none';
    };
    ECCH.Display.DOM.base.appendChild(infoToggler);

    var title = document.createElement('h3');
    title.className = 'title';
    title.innerText = 'Eygle Cookie Clicker Helper';
    title.style.textAlign = 'center';
    title.style.fontSize = '1.3em';
    title.style.fontWeight = 'bold';
    title.style.color = '#0288d1';
    title.style.paddingTop = '8px';
    title.style.paddingBottom = '4px';
    ECCH.Display.DOM.base.appendChild(title);

    var line = document.createElement('div');
    line.className = 'line';
    line.style.marginBottom = '12px';
    ECCH.Display.DOM.base.appendChild(line);

    ECCH.Display.DOM.body = document.createElement('div');
    ECCH.Display.DOM.body.className = 'text';
    ECCH.Display.DOM.base.appendChild(ECCH.Display.DOM.body);

    ECCH.Display.DOM.actions.doParent = document.createElement('div');
    ECCH.Display.DOM.actions.doParent.style.fontWeight = 'bold';
    ECCH.Display.DOM.actions.doParent.style.color = '#388e3c';
    ECCH.Display.DOM.actions.doParent.style.textAlign = 'center';
    ECCH.Display.DOM.actions.doParent.style.fontSize = '1.2em';
    ECCH.Display.DOM.actions.doParent.style.lineHeight = '1.3em';
    ECCH.Display.DOM.body.appendChild(ECCH.Display.DOM.actions.doParent);

    ECCH.Display.DOM.actions.nextParent = document.createElement('div');
    ECCH.Display.DOM.actions.nextParent.style.lineHeight = '1.5em';
    ECCH.Display.DOM.body.appendChild(ECCH.Display.DOM.actions.nextParent);

    var nextActionTitle = document.createElement('h3');
    nextActionTitle.style.fontWeight = 'bold';
    nextActionTitle.innerText = 'Next actions:';
    ECCH.Display.DOM.actions.nextParent.appendChild(nextActionTitle);

    function createBuyBtn(nbr) {
        ECCH.Display.DOM.buy['Btn' + nbr] = document.createElement('div');
        ECCH.Display.DOM.buy['Btn' + nbr].style.display = 'inline-block';
        ECCH.Display.DOM.buy['Btn' + nbr].style.border = '1px solid white';
        ECCH.Display.DOM.buy['Btn' + nbr].style.borderRadius = '5px';
        ECCH.Display.DOM.buy['Btn' + nbr].style.cursor = 'pointer';
        ECCH.Display.DOM.buy['Btn' + nbr].style.margin = '0 2px';
        ECCH.Display.DOM.buy['Btn' + nbr].style.padding = '2px 5px';
        ECCH.Display.DOM.buy['Btn' + nbr].innerText = "" + nbr;
        ECCH.Display.DOM.buy['Btn' + nbr].onclick = function() {
            ECCH.Utils.buyNext(nbr);
        };
        ECCH.Display.DOM.buy.parent.appendChild(ECCH.Display.DOM.buy['Btn' + nbr]);
    }

    ECCH.Display.DOM.buy.parent = document.createElement('div');
    ECCH.Display.DOM.buy.parent.style.position = 'absolute';
    ECCH.Display.DOM.buy.parent.style.top = '45px';
    ECCH.Display.DOM.buy.parent.style.right = '5px';
    ECCH.Display.DOM.body.appendChild(ECCH.Display.DOM.buy.parent);

    var label = document.createElement('span');
    label.style.marginRight = '3px';
    label.innerText = 'Buy';
    ECCH.Display.DOM.buy.parent.appendChild(label);

    createBuyBtn( 1);
    createBuyBtn(10);
    createBuyBtn(100);

    ECCH.Display.DOM.spellMode = document.createElement('div');
    ECCH.Display.DOM.spellMode.style.position = 'absolute';
    ECCH.Display.DOM.spellMode.style.top = '25px';
    ECCH.Display.DOM.spellMode.style.left = '5px';
    ECCH.Display.DOM.spellMode.style.padding = '0 5px';
    ECCH.Display.DOM.spellMode.style.border = '1px solid white';
    ECCH.Display.DOM.spellMode.style.borderRadius = '5px';
    ECCH.Display.DOM.spellMode.innerText = 'Spell mode ' + (ECCH.Config.spellMode ? 'on' : 'off');
    ECCH.Display.DOM.spellMode.onclick = function() {
        ECCH.Config.spellMode = !ECCH.Config.spellMode;
        ECCH.Display.DOM.spellMode.innerText = 'Spell mode ' + (ECCH.Config.spellMode ? 'on' : 'off');
    };
    ECCH.Display.DOM.base.appendChild(ECCH.Display.DOM.spellMode);

    ECCH.Display.DOM.infoBubble.parent = document.createElement('div');
    ECCH.Display.DOM.infoBubble.parent.className = 'framed';
    ECCH.Display.DOM.infoBubble.parent.style.display = 'none';
    ECCH.Display.DOM.infoBubble.parent.style.position = 'absolute';
    ECCH.Display.DOM.infoBubble.parent.style.bottom = '176px';
    ECCH.Display.DOM.infoBubble.parent.style.left = '36px';
    ECCH.Display.DOM.infoBubble.parent.style.width = '300px';
    ECCH.Display.DOM.infoBubble.parent.style.zIndex = '1001';
    ECCH.Display.DOM.infoBubble.parent.style.lineHeight = '1.4em';

    function addInfoBubbleItem(type) {
        ECCH.Display.DOM.infoBubble[type].parent = document.createElement('div');
        ECCH.Display.DOM.infoBubble[type].title = document.createElement('div');
        ECCH.Display.DOM.infoBubble[type].title.style.fontWeight = 'bold';
        ECCH.Display.DOM.infoBubble[type].bank = document.createElement('div');
        ECCH.Display.DOM.infoBubble[type].win = document.createElement('div');
        ECCH.Display.DOM.infoBubble[type].current = document.createElement('div');

        ECCH.Display.DOM.infoBubble.parent.appendChild(ECCH.Display.DOM.infoBubble[type].parent);
        ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].title);
        ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].bank);
        ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].win);
        ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].current);

        if (type !== 'grimoire') {
            ECCH.Display.DOM.infoBubble[type].separator = document.createElement('div');
            ECCH.Display.DOM.infoBubble[type].separator.className = 'line';
            ECCH.Display.DOM.infoBubble[type].parent.appendChild(ECCH.Display.DOM.infoBubble[type].separator);
        }
    }

    addInfoBubbleItem('lucky');
    addInfoBubbleItem('chain');
    addInfoBubbleItem('grimoire');

    document.getElementById('sectionMiddle').appendChild(ECCH.Display.DOM.base);
    document.getElementById('sectionMiddle').appendChild(ECCH.Display.DOM.icon);
    document.getElementById('sectionMiddle').appendChild(ECCH.Display.DOM.infoBubble.parent);
};

ECCH.Display.updateInfoBubble = function() {
    var frenzy = false;
    if (ECCH.Utils.goldenCookiesMode()) {
        frenzy = Game.Upgrades['Get lucky'].bought > 0;

        ECCH.Display.DOM.infoBubble.lucky.parent.style.display = 'block';
        ECCH.Display.DOM.infoBubble.lucky.title.innerText = 'Lucky Cookies' + (frenzy ? ' (frenzy)' : '') + ':';
        ECCH.Display.DOM.infoBubble.lucky.bank.innerText = 'Bank min: ' + Beautify(frenzy ? CM.Cache.LuckyFrenzy : CM.Cache.Lucky) + ' cookies';
        ECCH.Display.DOM.infoBubble.lucky.win.innerText = 'Gains: ' + Beautify(frenzy ? CM.Cache.LuckyRewardFrenzy : CM.Cache.LuckyReward) + ' cookies';
        // if (frenzy ? CM.Cache.LuckyFrenzy : CM.Cache.Lucky > Game.cookies) {
        //     ECCH.Display.DOM.infoBubble.lucky.current.style.display = 'block';
        //     ECCH.Display.DOM.infoBubble.lucky.current.innerText = 'Current gains: ' + + ' cookies';
        // } else {
        //     ECCH.Display.DOM.infoBubble.lucky.current.style.display = 'none';
        // }

        ECCH.Display.DOM.infoBubble.chain.parent.style.display = 'block';
        ECCH.Display.DOM.infoBubble.chain.title.innerText = 'Chain Cookies' + (frenzy ? ' (frenzy)' : '') + ':';
        ECCH.Display.DOM.infoBubble.chain.bank.innerText = 'Bank min: ' + Beautify(frenzy ? CM.Cache.ChainFrenzy : CM.Cache.Chain) + ' cookies';
        ECCH.Display.DOM.infoBubble.chain.win.innerText = 'Gains: ' + Beautify(frenzy ? CM.Cache.ChainFrenzyReward : CM.Cache.ChainReward) + ' cookies';
        ECCH.Display.DOM.infoBubble.chain.separator.style.display = ECCH.Utils.isGrimoireUsable() ? 'block' : 'none';
    } else {
        ECCH.Display.DOM.infoBubble.lucky.parent.style.display = 'none';
        ECCH.Display.DOM.infoBubble.chain.parent.style.display = 'none';
    }
    if (ECCH.Utils.isGrimoireUsable()) {
        var mult = frenzy && !Game.buffs.Frenzy ? 7 : 1;

        ECCH.Display.DOM.infoBubble.grimoire.parent.style.display = 'block';
        ECCH.Display.DOM.infoBubble.grimoire.title.innerText = 'Spell - Conjure Baked Goods' + (frenzy ? ' (frenzy)' : '') + ':';
        ECCH.Display.DOM.infoBubble.grimoire.bank.innerText = 'Bank min: ' + Beautify(Game.cookiesPs * 12000 * mult) + ' cookies';
        ECCH.Display.DOM.infoBubble.grimoire.win.innerText = 'Gains: ' + Beautify(Game.cookiesPs * 1800 * mult) + ' cookies'; // Between 15% of bank and 30 min of production
        if (Game.cookiesPs * 12000 * mult > Game.cookies) {
            var current = Math.min(Game.cookies * 0.15, Game.cookiesPs * 60 * 30);

            ECCH.Display.DOM.infoBubble.grimoire.current.style.display = 'block';
            ECCH.Display.DOM.infoBubble.grimoire.current.innerText = 'Current gains: ' + Beautify(current) + ' cookies';
        } else {
            ECCH.Display.DOM.infoBubble.grimoire.current.style.display = 'none';
        }
    } else {
        ECCH.Display.DOM.infoBubble.grimoire.parent.style.display = 'none';
    }
};

ECCH.Display.processAction = function(type, parent, htmlCb) {
    ECCH.Display.DOM.actions[parent].style.display = ECCH.Display.messages[type].length > 0 ? 'block' : 'none';
    for (var i = 0; i < ECCH.Display.messages[type].length; i++) {
        if (i > ECCH.Display.DOM.actions[type].length - 1) {
            var action = document.createElement('div');
            action.innerHTML = htmlCb(ECCH.Display.messages[type][i]);

            ECCH.Display.DOM.actions[type].push(action);
            ECCH.Display.DOM.actions[parent].appendChild(action);
        } else {
            ECCH.Display.DOM.actions[type][i].innerHTML = htmlCb(ECCH.Display.messages[type][i]);
        }
    }
    // Remove excess
    while (ECCH.Display.DOM.actions[type].length > ECCH.Display.messages[type].length) {
        ECCH.Display.DOM.actions[type][ECCH.Display.DOM.actions[type].length - 1].remove();
        ECCH.Display.DOM.actions[type].pop();
    }
};

ECCH.Display.processBuyBtns = function() {
    var canBuy = ECCH.Utils.canBuy();

    ECCH.Display.DOM.buy.parent.style.display = canBuy ? 'block' : 'none';

    if (canBuy) {
        var next = ECCH.Utils.getNextBuyable();

        if (!next) return;

        if (next.type === ECCH.BUY_TYPE.UPGRADE) {
            ECCH.Display.DOM.buy.Btn10.style.visibility = 'hidden';
            ECCH.Display.DOM.buy.Btn100.style.visibility = 'hidden';
        } else {
            ECCH.Display.DOM.buy.Btn10.style.visibility = ECCH.Calc.calculateBankMinToHave(next.item.getSumPrice(10)) <= Game.cookies ? 'visible' : 'hidden';
            ECCH.Display.DOM.buy.Btn100.style.visibility = ECCH.Calc.calculateBankMinToHave(next.item.getSumPrice(100)) <= Game.cookies ? 'visible' : 'hidden';
        }
    }
};

ECCH.Display.refresh = function () {
    ECCH.Display.processAction('doActions', 'doParent', function(item) {
        return item.doAction + (item.type === ECCH.TYPES.BUY ? ' ' + item.name : '') + ' now!';
    });
    ECCH.Display.processAction('nextActions', 'nextParent', function (item) {
        if (item.type === ECCH.TYPES.BUY) {
            var main = item.doAction + ' ' + item.name + ' in <acronym title="At ' + ECCH.Utils.getTime(item.time) + '">' + ECCH.Utils.formatDuration(item.time) + '</acronym>';
            var bankMin = ECCH.Calc.calculateBankMinToHave(item.price);
            var bankInfo = '<p style="color: grey; line-height: 1em;">Bank min: ' + Beautify(bankMin) + ' cookies</p>';
            var need = '<p style="color: grey; line-height: 1em;">Deficit: ' + Beautify(bankMin - Game.cookies) + ' cookies</p>';

            return main + bankInfo + need;
        }
        return item.doAction + ' in <acronym title="At ' + ECCH.Utils.getTime(item.time) + '">' + ECCH.Utils.formatDuration(item.time) + '</acronym>';
    });

    ECCH.Display.processBuyBtns();
    ECCH.Display.updateInfoBubble();
};
/**
 * Loop
 */
ECCH.loop = function () {
    var list = ECCH.Calc.getItemsSortedByTime();
    var hasPlayed = {};

    hasPlayed[ECCH.TYPES.BUY] = false;
    hasPlayed[ECCH.TYPES.GRIMOIRE] = false;
    hasPlayed[ECCH.TYPES.GARDEN] = false;

    ECCH.Display.messages.doActions = [];
    ECCH.Display.messages.nextActions = [];

    list.forEach(function (item) {
        if (item.time > 0) {
            ECCH.Display.messages.nextActions.push(item);
        } else {
            ECCH.Utils.playSound(item.type);
            hasPlayed[item.type] = true;
            ECCH.Display.messages.doActions.push(item);
        }
    });

    Object.keys(hasPlayed).forEach(function (type) {
        type = parseInt(type, 10);
        if (!hasPlayed[type]) {
            ECCH.Utils.getSoundByType(type).hasPlayed = false;
        }
    });

    ECCH.Display.refresh();
    setTimeout(ECCH.loop, ECCH.Config.INTERVAL);
};

ECCH.Config.reset();

ECCH.runDelayed = function () {
    if (typeof CM === 'object' && typeof CM.Cache === 'object'
        && typeof CM.Cache.Objects === 'object' && typeof CM.Cache.Upgrades === 'object') {
        ECCH.Display.init();
        ECCH.loop();
        console.log('Eygle Cookie Clicker Helper mod is loaded!');
    } else {
        setTimeout(ECCH.runDelayed, 500);
    }
};

ECCH.runDelayed();