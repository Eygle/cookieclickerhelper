# Cookie Clicker © Helper

This tool is an extension to [Cookie Monster](https://cookieclicker.fandom.com/wiki/Cookie_Monster_(JavaScript_Add-on\)) addon ([Releases](https://github.com/Aktanusa/CookieMonster/releases)).  
It makes a sound whenever:
* A building or an upgrade is purchasable taking in consideration the minimum of cookies you need to keep in bank for spells and/or Golden Cookies.
* The garden is empty or mature enough to need a soil change
* You can cast a `Diminish Ineptitude` and `Conjured Baked Goods` spells

## Installation
Copy/paste the following code in a chrome or firefox shortcut:
```
javascript:(function() {
    Game.LoadMod('https://aktanusa.github.io/CookieMonster/CookieMonster.js');
    Game.LoadMod('https://eygle.gitlab.io/cookieclickerhelper/EygleCCHelper.js');
}());
```

Or simply past this code in the developer console:

```javascript
Game.LoadMod('https://aktanusa.github.io/CookieMonster/CookieMonster.js');
Game.LoadMod('https://eygle.gitlab.io/cookieclickerhelper/EygleCCHelper.js');
```

## Commands

* `ECCH.goldenCookiesMode(true)` use golden cookies mode
* `ECCH.goldenCookiesMode(false)` do not use golden cookies mode (useful when you have temple slots )
* `ECCH.start()` start printing helps in console (periodically, every seconds by default)
* `ECCH.stop()` stop printing helps
* `ECCH.mute()` mute sounds
* `ECCH.unMute()` unMute sounds
