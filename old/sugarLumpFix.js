(function () {
	const excludes = ["Farm", "Temple", "Wizard tower"];
	let total = 0;

	function calculateCost(level) {
		let total = 0;

		for (let i = 1; i <= level; i++) {
			total += i;
		}

		return total;
	}

	for (const key in Game.Objects) {
		const building = Game.Objects[key];

		if (!excludes.includes(key)) {
			if (building.level > 0) {
				console.log(`${key} has ${building.level} sugar lumps which cost ${calculateCost(building.level)}`);
				total += calculateCost(building.level);
				building.level -= building.level;
			}
		}
	}

	console.log(`Sugar lumps overspent: ${total}`);

	if (total > 0) {
		Game.lumpsTotal -= total;
		Game.gainLumps(total);
	}
})();