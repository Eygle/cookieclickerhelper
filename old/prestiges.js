(function() {
    const excludes = ['Classic dairy selection'];
    let str = '';
    let args = [];

    for (const u of Game.PrestigeUpgrades
        .filter(p => p.unlocked === 0 && !excludes.includes(p.name))
        .sort((a, b) => a.getPrice() > b.getPrice() ? 1 : (a.getPrice() < b.getPrice() ? -1 : 0))) {
        str += `%c${u.name}%c - %c${Beautify(u.getPrice())}%c Heavenly chips\n`;
        str += `%c${u.desc}%c\n`;
        args.push('font-weight: bold', '', 'color: #388e3c;', '', 'color: #888', '');
    }

    args.unshift(str);
    console.clear();
    console.log.apply(console.log, args);
})();

// CSV
(function() {
    const excludes = ['Classic dairy selection'];
    let str = '';

    console.clear();
    for (const u of Game.PrestigeUpgrades
    .filter(p => p.unlocked === 0 && !excludes.includes(p.name))
    .sort((a, b) => a.getPrice() > b.getPrice() ? 1 : (a.getPrice() < b.getPrice() ? -1 : 0))) {
        str += `${u.name}|${u.getPrice()}|${u.desc}$\n`;
    }
    console.log(str);
})();
