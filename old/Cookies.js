/**
 * Cookie Clicker © Helper
 *
 * This script is here to help you choose the next enhancement you will want to purchase in Cookie Clicker © game
 * It take in consideration building and upgrades
 *
 * This is not a cheat, you can use it without modifying your scores in the game.
 * This script only read info from the game
 *
 * The game: https://orteil.dashnet.org/cookieclicker
 */
class CCHelper {
	constructor() {
		/**
		 * Refresh interval (in milliseconds)
		 * @type {number}
		 */
		this.INTERVAL = 1000;

		this.cancel = false;
		this.autoMode = false;
		this.debug = true;
		this.logs = [];
	}

	/**
	 * Start helping periodically
	 */
	start() {
		this.cancel = false;
		this.autoMode = true;
		this.next();
	}

	/**
	 * Stop helping
	 */
	stop() {
		this.cancel = true;
		this.autoMode = false;
	}

	/**
	 * Toggle debug mode
	 */
	toggleDebug() {
		this.debug = !this.debug;
	}

	/**
	 * Store all logs and print it at once (avoid blink effect in console)
	 * @param args
	 */
	log(...args) {
		this.logs.push(args);
	}

	/**
	 * Print all logs and clean the queue
	 */
	flushLogs() {
		console.clear();
		for (const log of this.logs) {
			console.log.apply(console.log, log);
		}
		this.logs = [];
	}

	/**
	 * Read all info and extract next enhancement to buy
	 */
	next() {
		// Cancel next call (if auto mode)
		if (this.cancel) {
			this.cancel = false;
			return;
		}

		const items = [...this.processBuildings(), ...this.processUpgrates()];

		if (this.debug) {
			this.log([...items.sort((a, b) => { return a.ratio < b.ratio ? -1 : (a.ratio > b.ratio ? 1 : 0) })]);
		}

		const next = items.sort((a, b) => { return a.ratio < b.ratio ? -1 : (a.ratio > b.ratio ? 1 : 0) }).splice(0, 1)[0];
		const secs = parseInt((next.price - Game.cookies) / Game.cookiesPs, 10);
		const duration = CCHelper.getDuration(secs);

		if (this.debug) {
			this.log(`%cCCS: ${CCHelper.formatNbr(Game.cookiesPs)} - nbr of cookies: ${CCHelper.formatNbr(Game.cookies)} - objective: ${CCHelper.formatNbr(next.price - Game.cookies)}`, 'color: grey;');
			this.log(`%csecs: ${secs}`, 'color: grey;');
		}

		this.log(`%cNext: ${next.name} - ${secs <= 0 ? 'Buy now!' : `in ${duration}`} - Price: ${CCHelper.formatNbr(next.price)}`, 'font-size: 1.2em; font-weight: bold');
		this.flushLogs();

		if (this.autoMode) {
			setTimeout(() => this.next(), this.INTERVAL);
		}
	}

	processBuildings() {
		return Game.ObjectsById.map(e => ({name: e.name, price: e.getPrice(), ratio: e.getPrice() / (e.cps(e) * Game.globalCpsMult), win: e.cps(e) * Game.globalCpsMult}));
	}

	processUpgrates() {
		const items = [];
		const multiplierRegex = /^Cookie production multiplier <b>\+([0-9]+)%<\/b>\./;
		const efficiencyRegex = /([a-zA-Z ]+) are <b>(twice|[0-9]+ times)<\/b> as efficient/;
		const cursorsRegex = /([a-zA-Z ]+) gains? <b>([^<]+)<\/b> cookies for each non\-cursor object owned/;
		const doubleGainsRegex = /([a-zA-Z ]+) gains? <b>([^<]+)<\/b> per ([a-zA-Z ]+)/;

		Game.UpgradesInStore.forEach(e => {
		 	if (multiplierRegex.test(e.desc)) {
		 		const matches = e.desc.match(multiplierRegex);
		 		const win = Game.cookiesPs * (parseInt(matches[1]) / 100);

	 			items.push({name: e.name, price: e.basePrice, ratio: e.basePrice / win, win});
		 	} else if (efficiencyRegex.test(e.desc)) {
				const matches = e.desc.match(efficiencyRegex);
				const target = CCHelper.getBuildingByName(matches[1]);

				if (!target) {
					this.log(`%c\t> Ignored ${e.name} (no target) - ${e.desc}`, 'color: #ffca28;');
					return;
				}

				const cps = target.cps(target) * target.amount * Game.globalCpsMult;

				if (matches[2] === 'twice') {
					const win = target.cps(target) * target.amount * Game.globalCpsMult;
					items.push({name: e.name, price: e.getPrice(), ratio: e.getPrice() / win, win});
				} else {
					const mult = parseInt(matches[2]);
					items.push({name: e.name, price: e.getPrice(), ratio: e.getPrice() / (cps * mult), win: cps * mult});
				}
		 	} else if (cursorsRegex.test(e.desc)) {
	 			const matches = e.desc.match(cursorsRegex);

	 			function getNbr(txt) {
	 				txt = txt.replace('+', '');
	 				txt = txt.replace(',', '');
	 				txt = txt.replace(' million', '000000');

	 				return parseFloat(txt, 10);
	 			}

	 			const mult = getNbr(matches[2]);
	 			let win = 0;
	 			Game.ObjectsById.forEach(e => {
	 				if (e.name !== 'Cursor') {
	 					win += e.amount * mult;
	 				}
	 			});

				items.push({name: e.name, price: e.getPrice(), ratio: e.getPrice() / win, win});
		 	} else if (doubleGainsRegex.test(e.desc)) {
		 		let desc = e.desc;
	 			this.log(e.desc);

				function getNbr(txt) {
	 				txt = txt.replace('+', '');
	 				txt = txt.replace('% Cps', '');

	 				return parseFloat(txt, 10) / 100;
	 			}

	 			function getTotal() {
	 				const matches = desc.match(doubleGainsRegex);
	 				desc = matches.input.substr(matches[0].length);
	 				const first = CCHelper.getBuildingByName(matches[1]);
	 				const seconds = CCHelper.getBuildingByName(matches[3]);

	 				if (!first || ! seconds) {
	 					throw new Error();
	 				}

	 				return (first.cps(first) * Game.globalCpsMult) * (getNbr(matches[2]) * seconds.amount);
	 			}

				try {
		 			let win = getTotal();
		 			win += getTotal();

					items.push({name: e.name, price: e.getPrice(), ratio: e.getPrice() / win, win});
		 		} catch (e) {
		 			this.log(`%c\t> Ignored ${e.name} - ${e.desc}`, 'color: #ffca28;');
		 		}
		 	} else {
		 		this.log(`%c\t> Ignored ${e.name} - ${e.desc}`, 'color: #ffca28;');
		 	}
		});

		return items;
	}

	static getBuildingByName(name) {
		name = name.toLowerCase();
		if (name === 'the mouse and cursors') {
			name = 'cursor'
		}

		if (name.endsWith('ies')) {
			name = name.substr(0, name.length - 3) + 'y';
		}

		return Game.ObjectsById.find(e => name.startsWith(e.name.toLowerCase()));
	}

	static formatNbr(nbr) {
		let min = 1;
		let max = 1000;
		const steps = [, 'thousands', 'millions', 'billions', 'trillions', 'quadrillions', 'quintillions'];

		function round(nbr) {
			return Math.floor(nbr * 1000) / 1000;
		}

		for (const step of steps) {
			if (nbr < max) {
				return round((nbr / min)) + (step ? ` ${step}` : '');
			}
			min = max;
			max *= 1000;
		}

		return round(nbr / (min / 1000)) + ` ${steps[steps.length - 1]}`;
	}

	static getDuration(s) {
		const date = new Date(null);
		date.setSeconds(s); // specify value for SECONDS here
		return date.toISOString().substr(11, 8);
	}
}

const helper = new CCHelper();
helper.start();