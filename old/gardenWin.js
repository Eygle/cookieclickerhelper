function humanize(nbr) {let min = 1;let max = 1000;const steps = [, 'thousands', 'millions', 'billions', 'trillions', 'quadrillions', 'quintillions'];function round(nbr) {return Math.floor(nbr * 1000) / 1000;}for (const step of steps) {if (nbr < max) {return round((nbr / min)) + (step ? ` ${step}` : '');}min = max;max *= 1000;}return round(nbr / (min / 1000)) + ` ${steps[steps.length - 1]}`;}

function gardenWin() {
	const NBR_OF_SLOTS = [,4,6,9,12,16,20,25,30,36][Game.scriptBindings['minigameScript-2'].level];
	const cost = NBR_OF_SLOTS * 60 * Game.cookiesPs;
	const gains = (NBR_OF_SLOTS * 0.1 * Game.cookiesPs + NBR_OF_SLOTS * 0.025 * Game.cookiesPs) * (8 * 15 * 60);

	console.clear();
	console.log('%cGarden win:', 'font-weight: bold;');
	console.log(`Costs: ${humanize(cost)}`);
	console.log(`Makes: ${humanize(gains)}`);
	console.log(`%cGains: ${humanize(gains - cost)}`, 'font-weight: bold;color: white;');
}

gardenWin();