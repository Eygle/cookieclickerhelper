(function () {
  const game = Game.Objects['Farm'].minigame;
  let emptySlots = 0;
  let matureSlots = 0;
  let growingSlots = 0;
  let nbrSlots = 0;

  for (var x = 0; x < 6; x++) {
    for (var y = 0; y < 6; y++) {
      if (game.isTileUnlocked(x, y)) {
        const tile = game.getTile(x, y);

        if (tile[0]) {
          const plant = game.plantsById[tile[0] - 1];

          // plant is growing
          if (plant.unlocked) {
            // plant is mature
            if (tile[1] >= game.plantsById[tile[0] - 1].mature) {
              matureSlots++;
              // would die in next round
              if (plant.ageTick + plant.ageTickR + tile[1] >= 100) {

              }
            }
            else {
              growingSlots++;
            }
          }
        }
        else {
          emptySlots++;
        }
        nbrSlots++;
      }
    }
  }

  const item = {
    type: this.TYPES.GARDEN,
    time: 0
  };

  if (emptySlots > 0 && matureSlots < nbrSlots / 2 && game.soilsById[game.soil].name === 'Clay') {
    // (near) empty garden
    item.doAction = 'Plant garden';
  } else if (emptySlots === 0 && matureSlots > 0 && game.soilsById[game.soil].name === 'Fertilizer') {
    // mature garden
    item.doAction = 'Change garden soil';
  }

  return item.doAction ? item : null;
})();
